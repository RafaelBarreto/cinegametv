using dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace infraestrutura.mapping
{
    public class PostagemMap : IEntityTypeConfiguration<Postagem>
    {
        public void Configure(EntityTypeBuilder<Postagem> builder)
        {
            builder.ToTable("Postagem");

            builder.HasOne(x => x.Filme);

            builder.HasMany(x => x.Imagens)
                   .WithOne()
                   .HasForeignKey(x => x.Id);
        }
    }
}