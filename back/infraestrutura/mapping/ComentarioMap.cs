using dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace infraestrutura.mapping
{
    public class ComentarioMap : IEntityTypeConfiguration<Comentario>
    {
        public void Configure(EntityTypeBuilder<Comentario> builder)
        {
            builder.ToTable("Comentario");

            builder.HasKey(x => x.Id)
                   .HasName("Id_Comentario");

            builder.HasOne(x => x.Usuario);

            builder.HasMany(x => x.Resposta)
                   .WithOne()
                   .HasForeignKey(x => x.Id)
                   .HasConstraintName("Fk_Resposta");

        }
    }
}