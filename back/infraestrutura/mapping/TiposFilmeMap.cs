using dominio.Entidades;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace infraestrutura.mapping
{
    public class TiposFilmeMap : IEntityTypeConfiguration<TiposFilmes>
    {
        public void Configure(EntityTypeBuilder<TiposFilmes> builder)
        {
            builder.ToTable("TiposFilme");
        }
    }
}