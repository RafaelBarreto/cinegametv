using dominio.Entidades;
using infraestrutura.mapping;
using Microsoft.EntityFrameworkCore;

namespace infraestrutura
{
    public class Contexto : DbContext
    {
        public Contexto(DbContextOptions<Contexto> options) : base(options) { }


        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Comentario> Comentario { get; set; }
        public DbSet<Filme> Filme { get; set; }
        public DbSet<Postagem> Postagem { get; set; }
        public DbSet<Resposta> Resposta { get; set; }
        public DbSet<Imagem> Imagem { get; set; }
        public DbSet<TiposFilmes> TiposFilmes { get; set; }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ComentarioMap());
            builder.ApplyConfiguration(new FilmeMap());
            builder.ApplyConfiguration(new ImagemMap());
            builder.ApplyConfiguration(new PostagemMap());
            builder.ApplyConfiguration(new RespostaMap());
            builder.ApplyConfiguration(new UsuarioMap());
            builder.ApplyConfiguration(new TiposFilmeMap());
        }
    }
}