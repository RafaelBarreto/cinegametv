using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace infraestrutura
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<Contexto>
    {
        public Contexto CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<Contexto>();
            builder.UseMySql(@"Server=127.0.0.1;Port=3306;Database=CineGameTv;Uid=CineGameTv;Pwd=cinegametv;");
            return new Contexto(builder.Options);
        }
    }
}