using System.Collections.Generic;
using System.Linq;
using dominio.Entidades;
using dominio.Interfaces.IRepositorio;

namespace infraestrutura.Repositorio
{
    public class PostagemRepositorio : IGenericCrud<Postagem>
    {
        Contexto Db;
        public PostagemRepositorio(Contexto contexto)
        {
            Db = contexto;
        }
        public Postagem adicionar(Postagem postagem)
        {
            Db.Postagem.Add(postagem);
            Db.SaveChanges();
            return postagem;
        }

        public bool atualizar(Postagem postagem)
        {
            Db.Postagem.Update(postagem);
            Db.SaveChanges();
            return true;
        }

        public List<Postagem> buscarTodos()
        {
            return Db.Postagem.Select(x => x).ToList();
        }

        public bool deletar(Postagem postagem)
        {
            Db.Postagem.Remove(postagem);
            Db.SaveChanges();
            return true;
        }

        public List<Postagem> GetLatestPost()
        {
            return Db.Postagem.Select(item => item)
                .OrderBy(item => item.Data)
                .Take(6)
                .ToList();
        }
    }
}