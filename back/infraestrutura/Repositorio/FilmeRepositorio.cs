using System.Collections.Generic;
using dominio.Entidades;
using dominio.Interfaces.IRepositorio;

namespace infraestrutura.Repositorio
{
    public class FilmeRepositorio : IGenericCrud<Filme>
    {
        readonly private Contexto _contexto;
        public FilmeRepositorio(Contexto ct)
        {
            _contexto = ct;
        }
        public Filme adicionar(Filme filme)
        {
            _contexto.Filme.Add(filme);
            _contexto.SaveChanges();
            return filme;
        }

        public bool atualizar(Filme Objeto)
        {
            throw new System.NotImplementedException();
        }

        public List<Filme> buscarTodos()
        {
            throw new System.NotImplementedException();
        }

        public bool deletar(Filme Objeto)
        {
            throw new System.NotImplementedException();
        }
    }
}