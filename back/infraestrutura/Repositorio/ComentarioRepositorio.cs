using System.Collections.Generic;
using System.Linq;
using dominio.Entidades;
using dominio.Interfaces.IRepositorio;

namespace infraestrutura.Repositorio
{
    public class ComentarioRepositorio : IGenericCrud<Comentario>, IComentarioRepositorio
    {
        Contexto Db;
        public ComentarioRepositorio(Contexto contexto)
        {
            Db = contexto;
        }

        public Comentario adicionar(Comentario comentario)
        {
            Db.Comentario.Add(comentario);
            Db.SaveChanges();
            return comentario;
        }

        public bool atualizar(Comentario comentario)
        {
            Db.Comentario.Update(comentario);
            Db.SaveChanges();
            return true;
        }

        public bool deletar(Comentario comentario)
        {
            Db.Comentario.Remove(comentario);
            Db.SaveChanges();
            return true;
        }

        public List<Comentario> buscarTodos()
        {
            return Db.Comentario.ToList();
        }

    }
}