using System.Collections.Generic;
using dominio.Entidades;
using dominio.Interfaces.IRepositorio;

namespace infraestrutura.Repositorio
{
    public class RespostaRepositorio : IGenericCrud<Resposta>, IRespostaRepositorio
    {
        Contexto Db;

        public RespostaRepositorio(Contexto contex)
        {
            Db = contex;
        }

        public Resposta adicionar(Resposta Objeto)
        {
            throw new System.NotImplementedException();
        }

        public bool atualizar(Resposta Objeto)
        {
            throw new System.NotImplementedException();
        }

        public List<Resposta> buscarTodos()
        {
            throw new System.NotImplementedException();
        }

        public bool deletar(Resposta Objeto)
        {
            throw new System.NotImplementedException();
        }
    }
}