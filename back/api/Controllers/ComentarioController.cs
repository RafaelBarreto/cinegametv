using dominio.Entidades;
using dominio.Interfaces.IRepositorio;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/[Controller]")]
    public class ComentarioController : Controller
    {
        IGenericCrud<Comentario> _comentarioRepositorio;
        public ComentarioController(IGenericCrud<Comentario> repositorio)
        {
            _comentarioRepositorio = repositorio;
        }

        [HttpGet("buscar-todos")]
        public IActionResult buscarComentarios()
        {
            var todosComentarios = _comentarioRepositorio.buscarTodos();
            if (todosComentarios != null) { return Ok(todosComentarios); }
            return BadRequest();
        }

        [HttpPost("salvar")]
        public IActionResult salvarComentario([FromBody]Comentario comentario)
        {
            _comentarioRepositorio.adicionar(comentario);
            return Ok();
        }


        [HttpPut("atualizar")]
        public IActionResult atualizarComentario([FromBody]Comentario comentario)
        {
            var atualizou = _comentarioRepositorio.atualizar(comentario);
            if (atualizou) { return Ok(atualizou); }
            return StatusCode(404);
        }

        [HttpDelete("deletar")]
        public IActionResult deletarComentario([FromBody]Comentario comentario)
        {
            var deletou = _comentarioRepositorio.deletar(comentario);
            if (deletou) { return Ok(deletou); }
            return StatusCode(404);
        }




    }
}