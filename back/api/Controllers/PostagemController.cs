using dominio.Entidades;
using infraestrutura.Repositorio;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/postagem")]
    public class PostagemController : Controller
    {
        PostagemRepositorio _repositorio;

        public PostagemController(PostagemRepositorio repositorio)
        {
            _repositorio = repositorio;
        }


        [HttpGet("buscar-todos")]
        public IActionResult buscarTodos()
        {
            var postagens = _repositorio.buscarTodos();
            if (postagens != null) { return Ok(postagens); }
            return BadRequest();
        }

        [HttpPost("adicionar")]
        public IActionResult novaPostagem([FromBody]Postagem post)
        {
            _repositorio.adicionar(post);
            return Ok();
        }

        [HttpPut("Atualizar")]
        public IActionResult atualizarPostagem([FromBody]Postagem post)
        {
            var resposta = _repositorio.atualizar(post);
            if (resposta) { return Ok(); }
            return BadRequest();
        }

        [HttpDelete("deletar")]
        public IActionResult deletarPostagem([FromBody]Postagem post)
        {
            var resposta = _repositorio.deletar(post);
            if (resposta) { return Ok(); }
            return BadRequest();
        }

        [HttpGet("ultimas")]
        public IActionResult GetLatestPost()
        {
            var resposta = _repositorio.GetLatestPost();
            return Ok(resposta);
        }

    }
}