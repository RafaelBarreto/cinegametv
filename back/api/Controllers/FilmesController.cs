using dominio.Entidades;
using dominio.Interfaces.IRepositorio;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers
{
    [Route("api/filme")]
    public class FilmesController : Controller
    {
        readonly private IGenericCrud<Filme> _repositorio;
        public FilmesController(IGenericCrud<Filme> repositorio)
        {
            _repositorio = repositorio;
        }

        [HttpPost, Route("cadastrar")]
        public IActionResult cadastrarFilme([FromBody]Filme filme)
        {
            var result = _repositorio.adicionar(filme);
            if (result != null) { return Ok(result); }
            return BadRequest(result);
        }


    }
}