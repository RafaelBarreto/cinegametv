﻿using System.IO;
using dominio.Entidades;
using dominio.Interfaces.IRepositorio;
using infraestrutura;
using infraestrutura.Repositorio;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace api
{
    public class Startup
    {
        IDesignTimeDbContextFactory<Contexto> contexto;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Contexto>();
            services.AddMvc();
            services.AddTransient<Contexto>();
            services.AddTransient<IGenericCrud<Comentario>, ComentarioRepositorio>();
            services.AddTransient<IGenericCrud<Resposta>, RespostaRepositorio>();
            services.AddTransient<IGenericCrud<Usuario>, UsuarioRepositorio>();
            services.AddTransient<IGenericCrud<Filme>, FilmeRepositorio>();

            services.AddCors(options =>
            {
                options.AddPolicy("allowAll", buider =>
                {
                    buider
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowAnyOrigin()
                        .AllowCredentials();
                });
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseCors("allowAll");
            app.UseMvc();

        }
    }
}
