namespace dominio.Enum
{
    public enum ClasseFilmeEnum
    {
        Aventura,
        acao,
        Comedia,
        Terror,
        Suspense,
        Animacao,
        Historia,
        Luta,
        Ficcao,
        Drama,
        Romance,
        Musical
    }
}