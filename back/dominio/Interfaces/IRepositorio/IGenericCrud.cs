using System.Collections.Generic;

namespace dominio.Interfaces.IRepositorio
{
    public interface IGenericCrud<T>
    {
        T adicionar(T Objeto);
        bool atualizar(T Objeto);
        bool deletar(T Objeto);
        List<T> buscarTodos();
    }
}