using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dominio.Entidades
{
    public class Comentario
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public Usuario Usuario { get; set; }
        public List<Resposta> Resposta { get; set; }
    }
}