using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dominio.Entidades
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Senha { get; set; }
        public Imagem ImageBase64 { get; set; }
    }
}