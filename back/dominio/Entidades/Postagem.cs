using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dominio.Entidades
{
    public class Postagem
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Texto { get; set; }
        public Filme Filme { get; set; }
        public List<Imagem> Imagens { get; set; }
        public DateTime Data { get; set; }
    }
}