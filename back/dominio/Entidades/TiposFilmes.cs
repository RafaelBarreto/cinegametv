using System.Collections.Generic;

namespace dominio.Entidades
{
    public class TiposFilmes
    {
        public int Id { get; set; }
        public string Descricao { get; set; }
    }
}