using System.ComponentModel.DataAnnotations;

namespace dominio.Entidades
{
    public class Resposta
    {
        public int Id { get; set; }
        public string Texto { get; set; }
        public Usuario Usuario { get; set; }
    }
}