namespace dominio.Entidades
{
    public class Imagem
    {
        public int Id { get; set; }
        public string Base64 { get; set; }
    }
}