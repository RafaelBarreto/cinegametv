using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace dominio.Entidades
{
    public class Filme
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Sinopse { get; set; }
        public string CapaBase64 { get; set; }
        public ICollection<TiposFilmes> Tipos { get; set; }
        public TiposFilmes tiposFilmes { get; set; }
        public int IdTiposFilmes { get; set; }

        public Filme()
        {
            Tipos = new List<TiposFilmes>();
        }
    }
}