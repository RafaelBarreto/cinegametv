import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AngularFontAwesomeModule } from 'angular-font-awesome/angular-font-awesome';
import { HttpModule } from '@angular/http'

// app modules import
import { routing } from './config/app.routing';
import { AppComponent } from './app.component';
import { BigPostComponent } from './home/post/big-post/big-post.component';
import { EmBreveComponent } from './em-breve/em-breve.component';
import { EmBrevePostagemComponent } from './em-breve/em-breve-postagem/em-breve-postagem.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ImageComponentComponent } from './home/image-component/image-component.component';
import { MateriaComponent } from './materia/materia.component';
import { MenuComponent } from './menu/menu.component';
import { MenuItensComponent } from './menu/menu-itens/menu-itens.component';
import { PostComponent } from './home/post/post.component';
import { SmallPostComponent } from './home/post/small-post/small-post.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    MenuComponent,
    ImageComponentComponent,
    MenuItensComponent,
    PostComponent,
    SmallPostComponent,
    BigPostComponent,
    EmBreveComponent,
    EmBrevePostagemComponent,
    MateriaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFontAwesomeModule,
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
