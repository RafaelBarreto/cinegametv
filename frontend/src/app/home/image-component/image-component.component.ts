import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-image-component',
  templateUrl: './image-component.component.html',
  styleUrls: ['./image-component.component.scss']
})
export class ImageComponentComponent implements OnInit {
  @Input() link: string;
  @Input() title: string;
  @Input() description: string;


  constructor() { }

  ngOnInit() {
  }

}
