import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

const url = `${environment.urlBase}/postagem`;

@Injectable()
export class PostService {

  constructor(
    private http: Http
  ) { }

  getLatestPosts() {
    return this.http.get(`${url}/ultimas`)
    .pipe( map( (item: Response) => item.json() ))
    .toPromise();
  }

}
