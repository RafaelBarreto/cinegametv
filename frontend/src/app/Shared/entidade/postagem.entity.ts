import { Imagem } from "./imagem.entity";
import { Filme } from "./filme.emtity";

export class Postagem {
    id: number;
    titulo: string;
    texto: string;
    filme: Filme;
    imagens: Imagem;
    data: string;
}