import { Imagem } from "./imagem.entity";

export class Filme {
    id: string;
    nome: string;
    sinopse: string;
    capaBase64: string;
    imagens: Imagem[];
}