import { Resposta } from './resposta.entity';
import { Usuario } from './usuario.entity';
export class Comentario {
    id: number
    texto: string
    usuario: Usuario
    resposta: Resposta[]
}