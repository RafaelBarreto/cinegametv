import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @ViewChild('menu') menu: ElementRef;
  @ViewChild('openMenu') openMenu: ElementRef;
  statusMenu = false;
  constructor() { }

  ngOnInit() {
  }

  toogleMenu(): void {
    const open: string = this.statusMenu ? '-250px' : '0px';
    const arrowPosition: string = this.statusMenu ? '0' : '250px';

    this.menu.nativeElement.style.left = open;
    this.openMenu.nativeElement.style.left = arrowPosition;
    this.statusMenu = !this.statusMenu;
  }

}
