import { Component, OnInit, Input } from '@angular/core';

let count = 0;

@Component({
  selector: 'app-menu-itens',
  templateUrl: './menu-itens.component.html',
  styleUrls: ['./menu-itens.component.scss']
})
export class MenuItensComponent implements OnInit {
  @Input() classe: string;
  @Input() texto: string;
  @Input() link: string;
  activeClass: boolean;
  id: number;

  constructor() {
    this.id = count++;
  }

  ngOnInit() {
  }
}
