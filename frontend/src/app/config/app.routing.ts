import { ModuleWithProviders } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { EmBreveComponent } from '../em-breve/em-breve.component';
import { MateriaComponent } from '../materia/materia.component';

const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent },
    { path: 'em-breve', component: EmBreveComponent },
    { path: ':post/materia/:id', component: MateriaComponent },
    { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

export const routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);
