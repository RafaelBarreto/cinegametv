import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-em-breve-postagem',
  templateUrl: './em-breve-postagem.component.html',
  styleUrls: ['./em-breve-postagem.component.scss']
})
export class EmBrevePostagemComponent implements OnInit {
  @Input() image: string;
  @Input() text: string;
  @Input() titulo: string;

  constructor() { }

  ngOnInit() {
  }

}
