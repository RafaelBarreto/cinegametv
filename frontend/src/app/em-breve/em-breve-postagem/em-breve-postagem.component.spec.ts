import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmBrevePostagemComponent } from './em-breve-postagem.component';

describe('EmBrevePostagemComponent', () => {
  let component: EmBrevePostagemComponent;
  let fixture: ComponentFixture<EmBrevePostagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmBrevePostagemComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmBrevePostagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
